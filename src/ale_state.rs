pub enum AleState {
    Idle,
    Call,
    Connected(TrafficMode),
    Scan,
    Sounding
}

pub enum TrafficMode {
    SingleSideBandVoice,
    DigitalCodec2Voice,
    FreeDVDigitalPackets
}

pub struct AleMachine {
    state: AleState,
}

impl AleMachine {
    pub fn handle_state(&mut self) {
        match self.state {
            AleState::Idle => {}
            AleState::Call => {}
            AleState::Connected => {}
            AleState::Scan => {}
            AleState::Sounding => {}
        }
    }
}
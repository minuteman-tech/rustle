extern crate ctrlc;

mod ale_state;

use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;
use ale_state::{AleState, AleMachine};

enum MyError {
    CtrlCError(ctrlc::Error),
    // add other variants for other errors your program might encounter
}

impl From<ctrlc::Error> for MyError {
    fn from(error: ctrlc::Error) -> Self {
        MyError::CtrlCError(error)
    }
}

// Implement std::error::Error for MyError
impl std::error::Error for MyError {}

// Implement std::fmt::Display for MyError
impl std::fmt::Display for MyError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            MyError::CtrlCError(err) => write!(f, "Ctrl+C error: {}", err),
        }
    }
}
pub trait ShutdownSignal {
    fn should_shutdown(&self) -> bool;
}

pub struct UnixShutdownSignal {
    running: Arc<AtomicBool>,
}
fn main() -> Result<(), MyError> {
    let shutdown_signal = UnixShutdownSignal::new()?;

    while !shutdown_signal.should_shutdown() {
        let state = AleState::Idle;

        let mut ale_machine = AleMachine {
            state: AleState::Idle,
        };

        loop {
            ale_machine.handle_state();
        }
    }

    Ok(())
}